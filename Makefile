#     Makefile: proj2.c
#      Project: IOS project 2
#   Created on: 26.4.2016
#       Author: Vit Solcik (xsolci00)
#  Institution: The Faculty of Information Technology (FIT)
#               Brno University of Technology
 

NAME = proj2

CFLAGS = -std=gnu99 -Wall -Wextra -Werror -pedantic
LDFLAGS = -lrt -pthread


make:
	gcc $(CFLAGS) $(NAME).c -o proj2 $(LDFLAGS)

clean:
	rm -f *.o core *.out proj2

