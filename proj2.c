/*
 *   File name: proj2.c
 *     Project: IOS project 2
 *  Created on: 26.4.2016
 *      Author: Vit Solcik (xsolci00)
 * Institution: The Faculty of Information Technology (FIT)
 *              Brno University of Technology
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <semaphore.h>
#include <fcntl.h>
#include <sys/shm.h>
#include <time.h>
#include <sys/mman.h>
#include <signal.h>


typedef struct{
    int P;      //count of passengers
    int C;      //capacity of car
    int PT;     //max delay of generating passenger
    int RT;     //max delay of car traveling
}args_struct;

typedef struct{
    int process_count;  //count of process to determine 
    int action_counter; //count of process actions
    int boarded;        //count of passengers in car
}share_struct;


void free_resources();
void close_semaphore();
void carHandler();
void passengerHandler();
void generatorHandler();
void parentHandler();
void wait_finish();
void sighandler();

int set_resources();
int car();
int passenger();
int imput();

int shm_args;
int shm_share;
args_struct *args;
share_struct *shared_mem;
FILE *output;

sem_t *memmory_protect;
sem_t *allBoarded;
sem_t *allProcessCreated;
sem_t *finished;
sem_t *finishReq;
sem_t *load;
sem_t *allUnload;
sem_t *run;

int main(int argc,char *argv[]){

    signal(SIGINT, sighandler);
    signal(SIGTERM, sighandler);

    int state;

    if(set_resources() != 0){
        fprintf(stderr, "Set resources error\n"); 
        free_resources();
        return 2;
    }

    if(imput(argc,argv) != 0){
        fprintf(stderr, "Invalid argument\n");   
        free_resources();
        return 1;   
    }

        //open output file and handle errors    
    if((output = fopen("proj2.out","w+")) == NULL){
        fprintf(stderr,"Error opening file\n");
        free_resources();
        return 2;
    }

    pid_t generator;
    if ((generator = fork()) < 0){
        fprintf(stderr, "Error while creating: generator\n"); 
        free_resources();
        exit(2);
    }
    if (generator == 0) { // generator code
        signal(SIGINT, sighandler);
        signal(SIGTERM, sighandler);


        //generating passengers
        for (int i = 1; i <= args->P; ++i){
            pid_t passengerID;
            
            if(args->PT != 0){usleep(rand() % (args->PT + 1));}

            if ((passengerID = fork()) < 0){
                fprintf(stderr, "Error while creating: passenger %d \n",i);; 
                //free_resources();
            kill(0,SIGTERM);
                exit(2);
            }
            if (passengerID == 0){// passenger code
                signal(SIGINT, sighandler);
                signal(SIGTERM, sighandler);
                passenger(i);
                
                exit(0);
            }
        }

        //release passengers and car at once
        sem_wait(finishReq);
        for (int i = 1; i <= (args->P+1); ++i){
            sem_post(finished);
        }

        //wait for passengers and car
        for(int i = 1;i <= (args->P+1) ;i++){
            wait(NULL);
        }

        exit(0);

    }
    else{
        //parent code
        
        //generating car
        pid_t carPID;
        if((carPID = fork()) < 0){
            signal(SIGINT, sighandler);
            signal(SIGTERM, sighandler);
            
            fprintf(stderr, "Error while creating: car \n");
            free_resources();
            exit(2);
        }
        if(carPID == 0){// car code

            car();

            exit(0);
        }

    }

    //optional waiting by PID:
    //waitpid(generator, NULL, 0);
    //waitpid(carPID, NULL, 0);

    wait(&state); //generator wait
    if(state != 0){
        printf("wait 1 chyba hlavni proces \n");
        free_resources(); 
        close_semaphore();
        return 2;
    }
    wait(&state);   //car wait
    if(state != 0){
        printf("wait 2 chyba hlavni proces \n");
        free_resources(); 
        close_semaphore();
        return 2;
    }

    close_semaphore();
    free_resources();

    return 0;
}

int passenger(int passengerID){
    //passenger create notofocation
    sem_wait(memmory_protect);
    fprintf(output, "%d\t: P %d\t: started \n",(shared_mem->action_counter)++, passengerID);
    fflush(output);
    shared_mem->process_count++;
    sem_post(memmory_protect); 

    //boarding into car (load = open)
    sem_wait(load);
    sem_wait(memmory_protect);
    fprintf(output, "%d\t: P %d\t: board \n",(shared_mem->action_counter)++, passengerID);
    fflush(output);
    sem_post(memmory_protect);
    //boarding in order
    sem_wait(memmory_protect);
    shared_mem->boarded++;
    if(shared_mem->boarded < args->C)
        fprintf(output, "%d\t: P %d\t: board order %d \n",(shared_mem->action_counter)++, passengerID, shared_mem->boarded);
    else{
        fprintf(output, "%d\t: P %d\t: board last \n",(shared_mem->action_counter)++, passengerID);
        sem_post(allBoarded);
    }
    fflush(output);
    sem_post(memmory_protect);

    //car running
    sem_wait(run);

    //unboarding passengers from car
    sem_wait(memmory_protect); // lock critical section
    fprintf(output, "%d\t: P %d\t: unboard \n",(shared_mem->action_counter)++, passengerID);
    fflush(output);
    sem_post(memmory_protect);
    //unboardind in order
    sem_wait(memmory_protect);
    shared_mem->boarded--;
    if(shared_mem->boarded > 0)
        fprintf(output, "%d\t: P %d\t: unboard order %d \n",(shared_mem->action_counter)++, passengerID, (args->C - shared_mem->boarded));
    else{
        fprintf(output, "%d\t: P %d\t: unboard last \n",(shared_mem->action_counter)++, passengerID);
        sem_post(allUnload);
    }
    fflush(output);
    sem_post(memmory_protect);   



    //waiting on other processes
    wait_finish();

    //passenger finished
    sem_wait(memmory_protect);
    fprintf(output, "%d\t: P %d\t: finished \n",(shared_mem->action_counter)++, passengerID);
    fflush(output);
    sem_post(memmory_protect); 

    return 0;
}

int car(){
    //car create notification
    sem_wait(memmory_protect);
    shared_mem->process_count++;
    fprintf(output, "%d\t: C 1\t: started \n",(shared_mem->action_counter)++ );
    fflush(output);
    sem_post(memmory_protect);

    //cyclest of car running
    for(int cycle_count = 0; cycle_count < (args->P/args->C);cycle_count++){
        //starting loading: passengers = C (car capacity)
        sem_wait(memmory_protect);  
        fprintf(output, "%d\t: C 1\t: load \n",(shared_mem->action_counter)++ );
        fflush(output);
        shared_mem->boarded = 0;
        for (int i = 1; i <= args->C; ++i){
          sem_post(load);
        }
        sem_post(memmory_protect);

        //run the car and random wait
        sem_wait(allBoarded);
        sem_wait(memmory_protect);
        fprintf(output, "%d\t: C 1\t: run \n",(shared_mem->action_counter)++ );
        fflush(output);
        sem_post(memmory_protect);
        if(args->RT != 0){usleep(rand() % (args->RT + 1));}

        //unload car
        sem_wait(memmory_protect);
        fprintf(output, "%d\t: C 1\t: unload \n",(shared_mem->action_counter)++ );
        fflush(output);
        for (int i = 1; i <= args->C; ++i){
            sem_post(run);
        }
        sem_post(memmory_protect);
        sem_wait(allUnload);
    }

    //waiting on other processes
    wait_finish();

    //car finished
    sem_wait(memmory_protect);
    fprintf(output, "%d\t: C 1\t: finished \n",(shared_mem->action_counter)++ );
    fflush(output);
    sem_post(memmory_protect);
    return 0;
}

int set_resources(){
    //shared memmory
    if( (shm_args = shm_open("/shmargs_xsolci00", O_CREAT | O_EXCL | O_RDWR, 0644)) == -1){return -2;}
    if( (shm_share = shm_open("/shmshare_xsolci00", O_CREAT | O_EXCL | O_RDWR, 0644)) == -1){return -2;}
    ftruncate(shm_args, sizeof(args_struct));
    ftruncate(shm_share, sizeof(share_struct));

    args = mmap(NULL, sizeof(args_struct), PROT_READ | PROT_WRITE, MAP_SHARED, shm_args, 0);
    args->P = 0;
    args->C = 0;
    args->PT = 0;
    args->RT = 0;

    shared_mem = mmap(NULL, sizeof(share_struct), PROT_READ | PROT_WRITE, MAP_SHARED, shm_share, 0);
    shared_mem->process_count = 0;
    shared_mem->action_counter = 1;
    shared_mem->boarded = 0;

    //semaphores
    if( (memmory_protect = sem_open("/memmoryProtect_xsolci00", O_CREAT | O_EXCL, 0666, 1)) == SEM_FAILED ){return -2;}
    if( (finished = sem_open("/finished_xsolci00", O_CREAT | O_EXCL, 0666, 0)) == SEM_FAILED ){return -2;}
    if( (finishReq = sem_open("/finishReq_xsolci00", O_CREAT | O_EXCL, 0666, 0)) == SEM_FAILED ){return -2;}
    if( (allProcessCreated = sem_open("/allProcessCreated_xsolci00", O_CREAT | O_EXCL, 0666, 0)) == SEM_FAILED ){return -2;}
    if( (load = sem_open("/load_xsolci00", O_CREAT | O_EXCL, 0666, 0)) == SEM_FAILED ){return -2;}
    if( (allBoarded = sem_open("/allBoarded_xsolci00", O_CREAT | O_EXCL, 0666, 0)) == SEM_FAILED ){return -2;}
    if( (allUnload = sem_open("/allUnload_xsolci00", O_CREAT | O_EXCL, 0666, 0)) == SEM_FAILED ){return -2;}
    if( (run = sem_open("/run_xsolci00", O_CREAT | O_EXCL, 0666, 0)) == SEM_FAILED ){return -2;}
    

    return 0;
}

void wait_finish(){
    //wait for creation of all processes
    sem_wait(memmory_protect);
    if(shared_mem->process_count < (args->P+1)){
        sem_post(memmory_protect);
        sem_wait(allProcessCreated);
    }else{
        for(int i = 1; i < (args->P+1); ++i){
            sem_post(allProcessCreated);
        }   
        sem_post(memmory_protect);
    }

    //wait until last process can finish
    //note: finishReq is openg in generator code
    sem_wait(memmory_protect);
    shared_mem->process_count--;
    if((shared_mem->process_count) > 0){
        sem_post(memmory_protect);
        sem_wait(finished);

    }else{
        sem_post(memmory_protect);
        sem_post(finishReq);
        sem_wait(finished);
    } 
}

//reading stdin , returns 1 on invalid argument
int imput(int argc,char *argv[]){
    
    if(argc != 5){
        return 1;
    }

    char *not_number;
    
    args->P = strtol(argv[1], &not_number,10);
    if (*not_number != '\0'){
        return 1;
    }
    args->C = strtol(argv[2], &not_number,10);
    if (*not_number != '\0'){
        return 1;
    }
    args->PT = strtol(argv[3], &not_number,10);
    if (*not_number != '\0'){
        return 1;
    }
    args->RT = strtol(argv[4], &not_number,10);
    if (*not_number != '\0'){
        return 1;
    }

    if(args->PT < 0 || args->PT > 5000){
        return 1;
    }

    if(args->RT < 0 || args->RT > 5000){
        return 1;
    }

    if(args->P <= 0 || args->C <= 0){
        return 1;
    }

    if ((args->P % args->C) != 0){
        return 1;
    }

    return 0;
}

void close_semaphore(){
    sem_close(memmory_protect);
    sem_close(allBoarded);
    sem_close(allProcessCreated);
    sem_close(finished);
    sem_close(finishReq);
    sem_close(load);
    sem_close(allUnload);
    sem_close(run);
}

void free_resources(){
    
    //free shared memmory
    shm_unlink("/shmargs_xsolci00");
    shm_unlink("/shmshare_xsolci00");
    if(output != NULL){fclose(output);}

    munmap(shared_mem, sizeof(share_struct));
    munmap(args, sizeof(args_struct));

    //unlink semaphores
    sem_unlink("/memmoryProtect_xsolci00");
    sem_unlink("/allBoarded_xsolci00");
    sem_unlink("/allProcessCreated_xsolci00");
    sem_unlink("/finished_xsolci00");
    sem_unlink("/finishReq_xsolci00");
    sem_unlink("/load_xsolci00");
    sem_unlink("/allUnload_xsolci00");
    sem_unlink("/run_xsolci00");

    //close memory
    if(close(shm_args) != 0){printf("Error while closing shared memmory\n"); exit(1);};
    if(close(shm_share) != 0){printf("Error while closing shared memmory\n"); exit(1);};

}

void sighandler(){
    kill(0,SIGTERM);
    printf("sighandler\n");
    close_semaphore();
    free_resources();
    exit(2);
}